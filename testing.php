<?php
require('config/global.php');
$Interface = isset($_POST['Interface']) ? $_POST['Interface'] : "eth0";//Interface

$json = array(
			"fail_tolerance" => 3,
			"hosts" => array(
				array(
				   "host" => 'cc.qntmnet.com',
				   "port" => 80,
				),
			),
			"method" => 'netcat',
			"interface" => $Interface,
			"timeout" => 5,
        );

$cmd="$NginXpython3 ".$PythonFilePath."healthcheck_service.py --no-service '".json_encode($json)."'";
$result = shell_exec($cmd);
echo $result;
return;
?>
