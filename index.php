<?php 
//Global Config File
require('config/global.php');

$resultConfigure = read_write_json_file("DataFile/Device_Setup.json","read","");
if($resultConfigure["Configure_Flag"]==0){
	header('location: quicksetup.php');
	exit;
}

//Proceed Login
$error_msg = @$_GET['error'];
if(isset($_POST['login']) && !empty($_POST)){
	require_once('controllers/LoginController.php');
	$Username = $_POST['Username'];
	$Password = md5($_POST['Password']);
	$login = LoginController::makeLogin($Username,$Password);
	if(empty($login)){
		$error_msg = "Invalid credentials";
	}else{
		$_SESSION['UserID']   = $login['id'];
		$_SESSION['Email']    = $login['Email'];
		$_SESSION['Username'] = $login['Username'];
		$_SESSION['Role']     = $login['Role'];
		if($resultConfigure["Configure_Flag"]==1){
			header('location: views/CloudConfig_Page.php');
			exit;
		}else{ 
			header('location: views/dashboard.php');
			exit;
		}
	}
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>QN-SDWAN</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="content/css/style.css">
  <link rel="stylesheet" href="content/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="content/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="content/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="content/css/AdminLTE.min.css">
  <link rel="stylesheet" href="content/css/login.css">
  <!-- iCheck 
  <link rel="stylesheet" href="plugins/iCheck/square/blue.css">-->
  <link rel="shortcut icon" href="content/images/favicon.png">

</head>
<body class="hold-transition">
	<div class="login-box">
		<div class="login-box-body" >
				<div class="login-logo"><a href="index.php"><img src="content/images/logo.png?v=08.18.0.234" class="qntmlogo"></a></div>
					 <p class="login-box-msg" id="CloudLoginP"><i class="fa fa-lock">&nbsp;</i><label style="color:#002855 !important;"> QN SD WAN Login</label></p>
					   <form method="post" action="">
						  	  <div class="form-group has-feedback" id="EmailDiv">
									<input type="text" name="Username" id="Username" class="form-control" placeholder="Username" maxlength="50" required>
									<span class="form-control-feedback"><i class="fa fa-envelope"></i></span>
							  </div>
							  <div class="form-group has-feedback" id="PasswordDiv">
									<input type="password" autocomplete="new-password" name="Password" id="Password" maxlength="50" class="form-control" placeholder="Password" required>
									<label class="form-control-feedback eyeicon" style="pointer-events:all">
									<span><i class="fa fa-eye" id="eye"></i></span>
								 </label>
							  </div>
							 <?php if(!empty($error_msg)){?>
							   <p style="color:#FF0000;font-size:15px;" class="alert alert-danger" ><?=$error_msg?></p>
							<?php }?>
							 <div class="form-group has-feedback">
								 <input type='submit' name="login" id="login" class="btn btn-primary btn-block btn-flat" style="font-size:15px" value='Login'>
							 </div>
					 </form>		
  </div> <!-- /.login-box-body -->
</div><!-- /.login-box -->
<script src="content/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="content/js/bootstrap.min.js"></script>
<script src="content/js/notify.min.js"></script>
<script>
	$(document).ready(function()
	{
		$("#Username").focus();
		$('.eyeicon').click(function(){
	  	   if ($("#Password").attr("type")=="password"){
			  if($("#Password").val()!="")
			  {
                 $("#Password").attr("type", "text");
		      }
           }else{
              $("#Password").attr("type", "password");
           }
        });
	});
</script>
</body>
</html>
